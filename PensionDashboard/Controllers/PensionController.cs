﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace PensionDashboard.Controllers
{
    public class PensionController : Controller
    {
        // GET: Pension
        public ActionResult Index()
        {
            return this.View("PensionDashboard");
        }
    }
}